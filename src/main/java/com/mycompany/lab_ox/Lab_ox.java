/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab_ox;

/**
 *
 * @author informatics
 */
public class Lab_ox {
    
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static  char currentPlayer = 'X';
    
    public static void main(String[] args) {
        printWelcome();
        printTable();
        printTurn();
    }
    
    private static void printWelcome() {
        System.out.println("Welcome to OX");
    }
    
    private static void printTable() {
        for(int i=0; i<3; i++) {
            for(int j=0;j<3;j++) {
                System.out.print(table[i][j] +" ");
            }
            System.out.println("");
        }
    }
    
    private static void printTurn() {
        System.out.println("Player "+currentPlayer+" turn");
    }
}
